# ABP.MicroServer

#### 介绍
一个简单的ABP框架微服务案例！

#### 截图效果

![输入图片说明](Rex.WebApp/Pic/%E7%99%BB%E5%BD%95.png)

![输入图片说明](Rex.WebApp/Pic/%E9%A6%96%E9%A1%B5.png)

![输入图片说明](Rex.WebApp/Pic/%E7%A7%9F%E6%88%B7.png)

![输入图片说明](Rex.WebApp/Pic/%E7%BB%84%E7%BB%87%E5%8D%95%E5%85%83.png)

![输入图片说明](Rex.WebApp/Pic/%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86.png)

![输入图片说明](Rex.WebApp/Pic/%E8%A7%92%E8%89%B2%E7%AE%A1%E7%90%86.png)

![输入图片说明](Rex.WebApp/Pic/%E8%8F%9C%E5%8D%95%E7%AE%A1%E7%90%86.png)

![输入图片说明](Rex.WebApp/Pic/%E5%AE%A1%E8%AE%A1%E6%97%A5%E5%BF%97.png)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


﻿namespace Rex.GatewayService.Extensions
{
    /// <summary>
    /// Yarp配置文件扩展
    /// </summary>
    /// <remarks>
    /// @创 建 者：Rex
    /// @创建日期：2022/10/11 19:53:20
    /// </remarks>
    public static class GatewayHostBuilderExtensions
    {
        public const string AppYarpJsonPath = "Config/Yarp.json";

        public static IHostBuilder AddYarpJson(
            this IHostBuilder hostBuilder, bool optional = true, bool reloadOnChange = true, string path = AppYarpJsonPath)
        {
            return hostBuilder.ConfigureAppConfiguration((_, builder) =>
            {
                builder.AddJsonFile(
                        path: AppYarpJsonPath,
                        optional: optional,
                        reloadOnChange: reloadOnChange
                    )
                    .AddEnvironmentVariables();
            });
        }
    }
}
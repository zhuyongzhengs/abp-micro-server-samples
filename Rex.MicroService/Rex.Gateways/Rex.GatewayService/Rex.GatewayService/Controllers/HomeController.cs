﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Rex.GatewayService.Controllers;

public class HomeController : Controller
{
    /// <summary>
    /// 首页
    /// </summary>
    /// <returns></returns>
    public IActionResult Index()
    {
        return View();
    }
}
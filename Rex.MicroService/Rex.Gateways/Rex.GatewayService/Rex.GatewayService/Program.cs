using Rex.GatewayService.Extensions;
using System;
using System.Diagnostics;

namespace Rex.GatewayService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Console.Title = "Yarp网关服务";

                var builder = WebApplication.CreateBuilder(args);
                builder.Host
                    // 添加Yarp的配置文件
                    .AddYarpJson()

                    // 日志配置
                    .ConfigureLogging((context, loggingbuilder) =>
                    {
                        // 1)过滤掉系统默认的一些日志
                        //loggingbuilder.AddFilter("System", LogLevel.Warning);
                        //loggingbuilder.AddFilter("Microsoft", LogLevel.Warning);

                        // 2)添加Log4Net
                        string path = Directory.GetCurrentDirectory() + "/Config/log4net.config";
                        loggingbuilder.AddLog4Net(path);
                    });

                builder.Services.AddCors(options =>
                {
                    options.AddDefaultPolicy(policy =>
                    {
                        string corsOrigins = builder.Configuration.GetSection("App").GetSection("CorsOrigins").Value;
                        policy
                            .WithOrigins(corsOrigins.Split(",", StringSplitOptions.RemoveEmptyEntries).ToArray() ?? Array.Empty<string>())
                            .SetIsOriginAllowedToAllowWildcardSubdomains()
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials();
                    });
                });

                // 添加Yarp反向代理ReverseProxy
                builder.Services.AddReverseProxy()
                    .LoadFromConfig(builder.Configuration.GetSection("ReverseProxy"));

                builder.Services.AddControllersWithViews();
                builder.Services.AddRazorPages().AddRazorRuntimeCompilation();

                /*
                builder.Services.AddSwaggerGen(options =>
                {
                    options.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Title = "Rex.GatewayService",
                        Version = "v1"
                    });
                    options.DocInclusionPredicate((docName, description) => true);
                    options.CustomSchemaIds(type => type.FullName);
                });
                */

                var app = builder.Build();

                /*
                // 添加内部服务的Swagger终点
                app.UseSwaggerUIWithYarp();  //访问网关地址，自动跳转到 /swagger 的首页
                app.UseRewriter(new RewriteOptions()
                    // Regex for "", "/" and "" (whitespace)
                    .AddRedirect("^(|\\|\\s+)$", "/swagger"));
                */

                app.UseCors();

                app.UseRouting();

                app.UseStaticFiles();

                app.UseEndpoints(endpoints =>
                {
                    // 添加Yarp终端Endpoints
                    endpoints.MapReverseProxy();
                });

                app.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                app.Run();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
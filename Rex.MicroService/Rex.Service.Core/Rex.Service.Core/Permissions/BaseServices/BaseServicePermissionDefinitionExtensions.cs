﻿using Volo.Abp.Authorization.Permissions;

namespace Rex.Service.Core.Permissions.BaseServices
{
    /// <summary>
    /// Base权限定义
    /// </summary>
    /// <remarks>
    /// @创 建 者：Rex
    /// @创建日期：2023/5/1 17:25:12
    /// </remarks>
    public static class BaseServicePermissionDefinitionExtensions
    {
        /// <summary>
        /// 添加首页权限
        /// </summary>
        /// <param name="baseGroup">组名称</param>
        public static void AddBaseHomePermissionDefine(this IPermissionDefinitionContext context, PermissionGroupDefinition baseGroup)
        {
            var permissionDefinition = baseGroup.AddPermission(BaseServicePermissions.Homes.Default);
        }

        /// <summary>
        /// 添加菜单权限
        /// </summary>
        /// <param name="baseGroup">组名称</param>
        public static void AddBaseMenuPermissionDefine(this IPermissionDefinitionContext context, PermissionGroupDefinition baseGroup)
        {
            var permissionDefinition = baseGroup.AddPermission(BaseServicePermissions.Menus.Default);
            permissionDefinition.AddChild(BaseServicePermissions.Menus.Update);
            permissionDefinition.AddChild(BaseServicePermissions.Menus.Create);
            permissionDefinition.AddChild(BaseServicePermissions.Menus.Delete);
        }

        /// <summary>
        /// 添加角色菜单权限
        /// </summary>
        /// <param name="baseGroup">组名称</param>
        public static void AddBaseRoleMenuPermissionDefine(this IPermissionDefinitionContext context, PermissionGroupDefinition baseGroup)
        {
            var permissionDefinition = baseGroup.AddPermission(BaseServicePermissions.RoleMenus.Default);
            permissionDefinition.AddChild(BaseServicePermissions.RoleMenus.Update);
            permissionDefinition.AddChild(BaseServicePermissions.RoleMenus.Create);
            permissionDefinition.AddChild(BaseServicePermissions.RoleMenus.Delete);
        }

        /// <summary>
        /// 添加组织单元权限
        /// </summary>
        /// <param name="baseGroup">组名称</param>
        public static void AddBaseOrganizationUnitDefine(this IPermissionDefinitionContext context, PermissionGroupDefinition baseGroup)
        {
            var permissionDefinition = baseGroup.AddPermission(BaseServicePermissions.OrganizationUnits.Default);
            permissionDefinition.AddChild(BaseServicePermissions.OrganizationUnits.Create);
            permissionDefinition.AddChild(BaseServicePermissions.OrganizationUnits.Update);
            permissionDefinition.AddChild(BaseServicePermissions.OrganizationUnits.Delete);
            permissionDefinition.AddChild(BaseServicePermissions.OrganizationUnits.ManagingUser);
            permissionDefinition.AddChild(BaseServicePermissions.OrganizationUnits.ManagingRole);
        }

        /// <summary>
        /// 添加审计日志权限
        /// </summary>
        /// <param name="baseGroup">组名称</param>
        public static void AddBaseAuditLoggingPermissionDefine(this IPermissionDefinitionContext context, PermissionGroupDefinition baseGroup)
        {
            var permissionDefinition = baseGroup.AddPermission(BaseServicePermissions.AuditLoggings.Default);
        }

        /// <summary>
        /// 添加安全日志权限
        /// </summary>
        /// <param name="baseGroup">组名称</param>
        public static void AddBaseSecurityLogPermissionDefine(this IPermissionDefinitionContext context, PermissionGroupDefinition baseGroup)
        {
            var permissionDefinition = baseGroup.AddPermission(BaseServicePermissions.SecurityLogs.Default);
        }
    }
}
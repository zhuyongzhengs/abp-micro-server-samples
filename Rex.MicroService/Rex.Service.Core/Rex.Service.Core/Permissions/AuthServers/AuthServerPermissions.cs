﻿namespace Rex.Service.Core.Permissions.AuthServices
{
    /// <summary>
    /// 授权服务权限
    /// </summary>
    /// <remarks>
    /// @创 建 者：Rex
    /// @创建日期：2023/5/1 17:43:00
    /// </remarks>
    public static class AuthServerPermissions
    {
        /// <summary>
        /// 组名称
        /// </summary>
        public const string GroupName = "AuthServer";
    }
}
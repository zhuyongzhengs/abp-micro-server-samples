﻿namespace Rex.Service.Core.Permissions.FormManagements
{
    /// <summary>
    /// 表单权限
    /// </summary>
    public static class FormManagementPermissions
    {
        public const string GroupName = "FormManagement";

        #region 表单

        /// <summary>
        /// 表单
        /// </summary>
        public static class Forms
        {
            /// <summary>
            /// 默认权限
            /// </summary>
            public const string Default = GroupName + ".Form";

            /// <summary>
            /// 创建权限
            /// </summary>
            public const string Create = Default + ".Create";

            /// <summary>
            /// 修改权限
            /// </summary>
            public const string Update = Default + ".Update";

            /// <summary>
            /// 删除权限
            /// </summary>
            public const string Delete = Default + ".Delete";
        }

        #endregion 表单
    }
}
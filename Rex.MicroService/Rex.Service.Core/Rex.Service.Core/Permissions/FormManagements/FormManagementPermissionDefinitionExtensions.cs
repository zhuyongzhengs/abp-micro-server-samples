﻿using Volo.Abp.Authorization.Permissions;

namespace Rex.Service.Core.Permissions.FormManagements
{
    /// <summary>
    /// 表单权限定义
    /// </summary>
    /// <remarks>
    /// @创 建 者：Rex
    /// @创建日期：2023/5/1 17:25:12
    /// </remarks>
    public static class FormManagementPermissionDefinitionExtensions
    {
        /// <summary>
        /// 添加表单权限
        /// </summary>
        /// <param name="formGroup">组名称</param>
        public static void AddFormManagementPermissionDefine(this IPermissionDefinitionContext context, PermissionGroupDefinition formGroup)
        {
            var permissionDefinition = formGroup.AddPermission(FormManagementPermissions.Forms.Default);
            permissionDefinition.AddChild(FormManagementPermissions.Forms.Update);
            permissionDefinition.AddChild(FormManagementPermissions.Forms.Create);
            permissionDefinition.AddChild(FormManagementPermissions.Forms.Delete);
        }
    }
}
﻿namespace Rex.Service.Core.Permissions.WorkflowServices
{
    /// <summary>
    /// 工作流服务权限
    /// </summary>
    public static class WorkflowPermissions
    {
        public const string GroupName = "WorkflowService";

        #region 书

        /// <summary>
        /// 书
        /// </summary>
        public static class Books
        {
            /// <summary>
            /// 默认权限
            /// </summary>
            public const string Default = GroupName + ".Books";

            /// <summary>
            /// 创建权限
            /// </summary>
            public const string Create = Default + ".Create";

            /// <summary>
            /// 修改权限
            /// </summary>
            public const string Update = Default + ".Update";

            /// <summary>
            /// 删除权限
            /// </summary>
            public const string Delete = Default + ".Delete";
        }

        #endregion 书

        #region 打印模板

        /// <summary>
        /// 打印模板
        /// </summary>
        public static class PrintTemplates
        {
            /// <summary>
            /// 默认权限
            /// </summary>
            public const string Default = GroupName + ".PrintTemplates";

            /// <summary>
            /// 创建权限
            /// </summary>
            public const string Create = Default + ".Create";

            /// <summary>
            /// 修改权限
            /// </summary>
            public const string Update = Default + ".Update";

            /// <summary>
            /// 删除权限
            /// </summary>
            public const string Delete = Default + ".Delete";
        }

        #endregion 打印模板
    }
}
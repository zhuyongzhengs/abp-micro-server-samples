﻿using Volo.Abp.Authorization.Permissions;

namespace Rex.Service.Core.Permissions.WorkflowServices
{
    /// <summary>
    /// 工作流权限定义
    /// </summary>
    /// <remarks>
    /// @创 建 者：Rex
    /// @创建日期：2023/5/1 17:25:12
    /// </remarks>
    public static class WorkflowPermissionDefinitionExtensions
    {
        /// <summary>
        /// 添加书权限
        /// </summary>
        /// <param name="wfGroup">组名称</param>
        public static void AddWorkflowBookPermissionDefine(this IPermissionDefinitionContext context, PermissionGroupDefinition wfGroup)
        {
            var permissionDefinition = wfGroup.AddPermission(WorkflowPermissions.Books.Default);
            permissionDefinition.AddChild(WorkflowPermissions.Books.Update);
            permissionDefinition.AddChild(WorkflowPermissions.Books.Create);
            permissionDefinition.AddChild(WorkflowPermissions.Books.Delete);
        }

        /// <summary>
        /// 添加打印模板权限
        /// </summary>
        /// <param name="wfGroup">组名称</param>
        public static void AddWorkflowPrintTemplatePermissionDefine(this IPermissionDefinitionContext context, PermissionGroupDefinition wfGroup)
        {
            var permissionDefinition = wfGroup.AddPermission(WorkflowPermissions.PrintTemplates.Default);
            permissionDefinition.AddChild(WorkflowPermissions.PrintTemplates.Update);
            permissionDefinition.AddChild(WorkflowPermissions.PrintTemplates.Create);
            permissionDefinition.AddChild(WorkflowPermissions.PrintTemplates.Delete);
        }
    }
}
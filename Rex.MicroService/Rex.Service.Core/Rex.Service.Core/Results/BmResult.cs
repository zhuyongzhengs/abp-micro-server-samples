﻿namespace Rex.Service.Core.Results
{
    /// <summary>
    /// (Vue)后台管理返回结果
    /// </summary>
    [Serializable]
    public class BmResult
    {
        /// <summary>
        /// 消息
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// 状态码
        /// </summary>
        public ResultCode Code { get; private set; }

        /// <summary>
        /// 数据
        /// </summary>
        public dynamic Data { get; private set; }

        /// <summary>
        /// 后台管理返回结果
        /// </summary>
        public BmResult(dynamic data = default)
        {
            this.Message = "Success";
            this.Code = ResultCode.Success;
            this.Data = data;
        }

        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="message">消息</param>
        public void SetSuccess(dynamic data, string message = "Success")
        {
            this.Data = data;
            this.Code = ResultCode.Success;
            this.Message = message;
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="message">消息</param>
        public void SetFail(string message = "Fail")
        {
            this.Code = ResultCode.Fail;
            this.Message = message;
        }

        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="message">消息</param>
        public void SetError(string message = "Error")
        {
            this.Code = ResultCode.Error;
            this.Message = message;
        }
    }
}
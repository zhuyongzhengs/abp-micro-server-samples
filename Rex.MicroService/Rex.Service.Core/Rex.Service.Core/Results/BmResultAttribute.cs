﻿namespace Rex.Service.Core.Results
{
    /// <summary>
    /// 统一(Vue)后台管理返回结果
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class BmResultAttribute : Attribute
    {
        // ...
    }
}
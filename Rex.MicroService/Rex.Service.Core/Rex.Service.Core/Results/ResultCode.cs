﻿namespace Rex.Service.Core.Results
{
    /// <summary>
    /// 结果(状态)码
    /// </summary>
    [Serializable]
    public enum ResultCode
    {
        /// <summary>
        /// 错误(异常)
        /// </summary>
        Error = -1,

        /// <summary>
        /// 成功
        /// </summary>
        Success = 0,

        /// <summary>
        /// 失败
        /// </summary>
        Fail = 1
    }
}
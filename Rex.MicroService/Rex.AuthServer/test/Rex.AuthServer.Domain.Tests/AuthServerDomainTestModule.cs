﻿using Rex.AuthServer.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Rex.AuthServer;

[DependsOn(
    typeof(AuthServerEntityFrameworkCoreTestModule)
    )]
public class AuthServerDomainTestModule : AbpModule
{

}

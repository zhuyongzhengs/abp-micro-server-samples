﻿using Volo.Abp;

namespace Rex.AuthServer.EntityFrameworkCore;

public abstract class AuthServerEntityFrameworkCoreTestBase : AuthServerTestBase<AuthServerEntityFrameworkCoreTestModule>
{

}

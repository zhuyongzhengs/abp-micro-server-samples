﻿using Volo.Abp.Modularity;

namespace Rex.AuthServer;

[DependsOn(
    typeof(AuthServerApplicationModule),
    typeof(AuthServerDomainTestModule)
    )]
public class AuthServerApplicationTestModule : AbpModule
{

}

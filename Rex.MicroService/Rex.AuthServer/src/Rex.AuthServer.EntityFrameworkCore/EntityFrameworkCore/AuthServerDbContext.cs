﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.OpenIddict;
using Volo.Abp.OpenIddict.Applications;
using Volo.Abp.OpenIddict.Authorizations;
using Volo.Abp.OpenIddict.EntityFrameworkCore;
using Volo.Abp.OpenIddict.Scopes;
using Volo.Abp.OpenIddict.Tokens;

namespace Rex.AuthServer.EntityFrameworkCore;

[ReplaceDbContext(typeof(IOpenIddictDbContext))]
[ConnectionStringName(AuthServerConsts.ConnectionStringName)]
public class AuthServerDbContext :
    AbpDbContext<AuthServerDbContext>,
    IOpenIddictDbContext
{
    /* Add DbSet properties for your Aggregate Roots / Entities here. */

    #region OpenIddict授权

    public DbSet<OpenIddictApplication> Applications { get; set; }

    public DbSet<OpenIddictAuthorization> Authorizations { get; set; }

    public DbSet<OpenIddictScope> Scopes { get; set; }

    public DbSet<OpenIddictToken> Tokens { get; set; }

    #endregion OpenIddict授权

    public AuthServerDbContext(DbContextOptions<AuthServerDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        /* Include modules to your migration db context */

        #region 授权服务OpenIddict

        AbpOpenIddictDbProperties.DbTablePrefix = AuthServerConsts.DefaultDbTablePrefix;
        AbpOpenIddictDbProperties.DbSchema = AuthServerConsts.DefaultDbSchema;
        builder.ConfigureOpenIddict();

        #endregion 授权服务OpenIddict

        /* Configure your own tables/entities inside here */

        //builder.Entity<YourEntity>(b =>
        //{
        //    b.ToTable(AuthServerConsts.DbTablePrefix + "YourEntities", AuthServerConsts.DbSchema);
        //    b.ConfigureByConvention(); //auto configure for the base class props
        //    //...
        //});
    }
}
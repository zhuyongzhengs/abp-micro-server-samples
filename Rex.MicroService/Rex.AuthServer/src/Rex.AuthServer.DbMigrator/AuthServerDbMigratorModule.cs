﻿using Rex.AuthServer.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace Rex.AuthServer.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(AuthServerEntityFrameworkCoreModule),
    typeof(AuthServerApplicationContractsModule)
    )]
public class AuthServerDbMigratorModule : AbpModule
{

}

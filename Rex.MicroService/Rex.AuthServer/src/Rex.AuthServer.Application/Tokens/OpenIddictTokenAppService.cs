﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.OpenIddict.Tokens;

namespace Rex.AuthServer.Tokens;

/// <summary>
/// 授权Token服务
/// </summary>
[Dependency(ServiceLifetime.Singleton)]
[Authorize]
public class OpenIddictTokenAppService : CrudAppService<OpenIddictToken, OpenIddictTokenDto, Guid, PagedAndSortedResultRequestDto, CreateOpenIddictTokenDto, UpdateOpenIddictTokenDto>, IOpenIddictTokenAppService
{
    public IOpenIddictTokenRepository OpenIddictTokenRepository { get; set; }
    private readonly IRepository<OpenIddictToken, Guid> _openIddictTokenRepository;

    public OpenIddictTokenAppService(IRepository<OpenIddictToken, Guid> repository) : base(repository)
    {
        _openIddictTokenRepository = repository;
    }

    /// <summary>
    /// 创建授权Token
    /// </summary>
    /// <param name="input">授权TokenDto</param>
    /// <returns></returns>
    public override async Task<OpenIddictTokenDto> CreateAsync(CreateOpenIddictTokenDto input)
    {
        // 映射授权Token
        OpenIddictToken openIddictToken = ObjectMapper.Map<CreateOpenIddictTokenDto, OpenIddictToken>(input);

        // 保存
        openIddictToken = await _openIddictTokenRepository.InsertAsync(openIddictToken);

        // 映射授权TokenDto
        return ObjectMapper.Map<OpenIddictToken, OpenIddictTokenDto>(openIddictToken);
    }

    /// <summary>
    /// 获取授权Token
    /// </summary>
    /// <param name="applicationId">应用程序ID</param>
    /// <param name="subject">主题ID</param>
    /// <param name="status">状态</param>
    /// <param name="type">类型</param>
    /// <returns></returns>
    public async Task<List<OpenIddictTokenDto>> GetTokensAsync(Guid applicationId, string subject = "", string status = "", string type = "")
    {
        Expression<Func<OpenIddictToken, bool>> authorizationExpression = p => p.ApplicationId == applicationId;
        if (!subject.IsNullOrEmpty())
        {
            authorizationExpression = authorizationExpression.And(p => p.Subject.Equals(subject));
        }
        if (!status.IsNullOrEmpty())
        {
            authorizationExpression = authorizationExpression.And(p => p.Status.Equals(status));
        }
        if (!type.IsNullOrEmpty())
        {
            authorizationExpression = authorizationExpression.And(p => p.Type.Equals(type));
        }
        List<OpenIddictToken> openIddictTokenList = await _openIddictTokenRepository.GetListAsync(authorizationExpression);
        return ObjectMapper.Map<List<OpenIddictToken>, List<OpenIddictTokenDto>>(openIddictTokenList);
    }
}
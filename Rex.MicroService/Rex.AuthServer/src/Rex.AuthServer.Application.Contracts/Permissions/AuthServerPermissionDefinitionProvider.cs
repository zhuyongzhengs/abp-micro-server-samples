﻿using Rex.AuthServer.Localization;
using Rex.Service.Core.Permissions.BaseServices;
using Rex.Service.Core.Permissions.FormManagements;
using Rex.Service.Core.Permissions.WorkflowServices;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace Rex.AuthServer.Permissions;

public class AuthServerPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var baseGroup = context.AddGroup(BaseServicePermissions.GroupName);
        var wfGroup = context.AddGroup(WorkflowPermissions.GroupName);
        var formGroup = context.AddGroup(FormManagementPermissions.GroupName);

        #region 首页定义

        context.AddBaseHomePermissionDefine(baseGroup);

        #endregion 首页定义

        #region 菜单定义

        context.AddBaseMenuPermissionDefine(baseGroup);

        #endregion 菜单定义

        #region 角色菜单定义

        context.AddBaseRoleMenuPermissionDefine(baseGroup);

        #endregion 角色菜单定义

        #region 组织单元定义

        context.AddBaseOrganizationUnitDefine(baseGroup);

        #endregion 组织单元定义

        #region 表单定义

        context.AddFormManagementPermissionDefine(formGroup);

        #endregion 表单定义

        #region 书定义

        context.AddWorkflowBookPermissionDefine(wfGroup);

        #endregion 书定义

        #region 打印模板定义

        context.AddWorkflowPrintTemplatePermissionDefine(wfGroup);

        #endregion 打印模板定义

        #region 审计日志定义

        context.AddBaseAuditLoggingPermissionDefine(baseGroup);

        #endregion 审计日志定义

        #region 安全日志定义

        context.AddBaseSecurityLogPermissionDefine(baseGroup);

        #endregion 安全日志定义
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<AuthServerResource>(name);
    }
}
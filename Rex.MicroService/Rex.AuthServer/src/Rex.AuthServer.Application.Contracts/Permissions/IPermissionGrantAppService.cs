﻿using System;
using System.Threading.Tasks;

namespace Rex.AuthServer.Permissions
{
    /// <summary>
    /// 授权接口服务
    /// </summary>
    /// <remarks>
    /// @创 建 者：Rex
    /// @创建日期：2023/4/18 23:01:02
    /// </remarks>
    public interface IPermissionGrantAppService
    {
        /// <summary>
        /// 添加角色权限
        /// </summary>
        /// <param name="roleName">角色名称</param>
        /// <param name="permission">权限</param>
        /// <returns></returns>
        public Task CreateRolePermissionAsync(string roleName, string permission);

        /// <summary>
        /// 添加用户权限
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="permission">权限</param>
        /// <returns></returns>
        public Task CreateUserPermissionAsync(Guid userId, string permission);

        /// <summary>
        /// 添加客户端权限
        /// </summary>
        /// <param name="clientName">客户端名称</param>
        /// <param name="permission">权限</param>
        /// <returns></returns>
        public Task CreateClientPermissionAsync(string clientName, string permission);
    }
}
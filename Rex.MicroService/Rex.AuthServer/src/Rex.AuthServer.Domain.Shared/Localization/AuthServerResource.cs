﻿using Volo.Abp.Localization;

namespace Rex.AuthServer.Localization;

[LocalizationResourceName("AuthServer")]
public class AuthServerResource
{

}

﻿using System.Threading.Tasks;

namespace Rex.AuthServer.Data;

public interface IAuthServerDbSchemaMigrator
{
    Task MigrateAsync();
}

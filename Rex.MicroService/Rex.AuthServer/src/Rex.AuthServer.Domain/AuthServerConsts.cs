﻿namespace Rex.AuthServer;

public static class AuthServerConsts
{
    #region 默认(授权)前缀

    public const string DefaultDbTablePrefix = "Auth_";
    public const string DefaultDbSchema = null;

    #endregion 默认(授权)前缀

    #region 系统前缀

    public const string SysDbTablePrefix = "Sys_";
    public const string SysDbSchema = null;

    #endregion 系统前缀

    #region 连接字符串

    public const string ConnectionStringName = "Default";

    #endregion 连接字符串
}
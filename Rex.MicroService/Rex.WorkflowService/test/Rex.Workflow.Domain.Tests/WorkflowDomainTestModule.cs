﻿using Rex.Workflow.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Rex.Workflow;

[DependsOn(
    typeof(WorkflowEntityFrameworkCoreTestModule)
    )]
public class WorkflowDomainTestModule : AbpModule
{

}

﻿using Volo.Abp.Modularity;

namespace Rex.Workflow;

[DependsOn(
    typeof(WorkflowApplicationModule),
    typeof(WorkflowDomainTestModule)
    )]
public class WorkflowApplicationTestModule : AbpModule
{

}

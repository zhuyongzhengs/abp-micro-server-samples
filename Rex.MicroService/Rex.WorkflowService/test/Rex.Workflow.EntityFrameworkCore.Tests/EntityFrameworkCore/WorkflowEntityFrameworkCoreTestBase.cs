﻿using Volo.Abp;

namespace Rex.Workflow.EntityFrameworkCore;

public abstract class WorkflowEntityFrameworkCoreTestBase : WorkflowTestBase<WorkflowEntityFrameworkCoreTestModule>
{

}

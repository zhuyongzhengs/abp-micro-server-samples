﻿using Localization.Resources.AbpUi;
using Rex.Workflow.Localization;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement.HttpApi;
using Volo.Abp.SettingManagement;
using Volo.Abp.TenantManagement;
using XCZ;

namespace Rex.Workflow;

[DependsOn(
    typeof(WorkflowApplicationContractsModule),
    typeof(AbpPermissionManagementHttpApiModule),
    typeof(AbpTenantManagementHttpApiModule),
    typeof(AbpFeatureManagementHttpApiModule),
    typeof(AbpSettingManagementHttpApiModule),
    typeof(FormHttpApiModule),
    typeof(FlowHttpApiModule)
    )]
public class WorkflowHttpApiModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        ConfigureLocalization();
    }

    private void ConfigureLocalization()
    {
        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Get<WorkflowResource>()
                .AddBaseTypes(
                    typeof(AbpUiResource)
                );
        });
    }
}
﻿using Rex.Workflow.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Rex.Workflow.Controllers;

/* Inherit your controllers from this class.
 */
public abstract class WorkflowController : AbpControllerBase
{
    protected WorkflowController()
    {
        LocalizationResource = typeof(WorkflowResource);
    }
}

﻿using System;

namespace Rex.Workflow.Models.Test;

public class TestModel
{
    public string Name { get; set; }

    public DateTime BirthDate { get; set; }
}

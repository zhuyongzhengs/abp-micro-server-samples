using Volo.Abp.Application.Dtos;

namespace Rex.Workflow.Books.Dto
{
    public class GetBookInputDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
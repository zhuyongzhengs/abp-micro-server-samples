﻿using Rex.Service.Core.Permissions.FormManagements;
using Rex.Service.Core.Permissions.WorkflowServices;
using Rex.Workflow.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace Rex.Workflow.Permissions;

public class WorkflowPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var wfGroup = context.AddGroup(WorkflowPermissions.GroupName);

        #region 书定义

        context.AddWorkflowBookPermissionDefine(wfGroup);

        #endregion 书定义

        #region 打印模板定义

        context.AddWorkflowPrintTemplatePermissionDefine(wfGroup);

        #endregion 打印模板定义
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<WorkflowResource>(name);
    }
}
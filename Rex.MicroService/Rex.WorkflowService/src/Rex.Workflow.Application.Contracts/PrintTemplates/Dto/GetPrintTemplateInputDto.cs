using System;
using Volo.Abp.Application.Dtos;

namespace Rex.Workflow.PrintTemplates.Dto
{
    public class GetPrintTemplateInputDto : PagedAndSortedResultRequestDto
    {
        public Guid? Id { get; set; }

        public string Filter { get; set; }
    }
}
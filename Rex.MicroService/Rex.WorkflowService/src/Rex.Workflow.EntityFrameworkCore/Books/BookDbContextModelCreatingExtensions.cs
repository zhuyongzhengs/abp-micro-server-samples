﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Rex.Workflow.Books
{
    /// <summary>
    /// 图书数据库上下文创建扩展
    /// </summary>
    public static class BookDbContextModelCreatingExtensions
    {
        public static void ConfigureBook(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            // 书
            builder.Entity<Book>(b =>
            {
                b.ToTable(WorkflowConsts.DefaultDbTablePrefix + "Books", WorkflowConsts.DefaultDbSchema);
                b.ConfigureByConvention();
                b.Property(x => x.Name).IsRequired();
            });
        }
    }
}
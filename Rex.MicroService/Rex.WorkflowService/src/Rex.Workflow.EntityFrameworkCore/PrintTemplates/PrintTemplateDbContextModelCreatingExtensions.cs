﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Rex.Workflow.PrintTemplates
{
    /// <summary>
    /// 打印模板数据库上下文创建扩展
    /// </summary>
    public static class PrintTemplateDbContextModelCreatingExtensions
    {
        public static void ConfigurePrintTemplate(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            // 打印模板
            builder.Entity<PrintTemplate>(b =>
            {
                b.ToTable(WorkflowConsts.DefaultDbTablePrefix + "PrintTemplates", WorkflowConsts.DefaultDbSchema);
                b.ConfigureByConvention();
                b.Property(x => x.Name).IsRequired();
            });
        }
    }
}
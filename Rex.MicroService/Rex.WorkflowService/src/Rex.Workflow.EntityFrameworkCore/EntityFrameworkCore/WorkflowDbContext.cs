﻿using Microsoft.EntityFrameworkCore;
using Rex.Workflow.Books;
using Rex.Workflow.PrintTemplates;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using XCZ.EntityFrameworkCore;

namespace Rex.Workflow.EntityFrameworkCore;

[ConnectionStringName(WorkflowConsts.ConnectionStringName)]
public class WorkflowDbContext :
    AbpDbContext<WorkflowDbContext>
{
    /* Add DbSet properties for your Aggregate Roots / Entities here. */

    /// <summary>
    /// 书
    /// </summary>
    public DbSet<Book> Book { get; set; }

    /// <summary>
    /// 打印模板
    /// </summary>
    public DbSet<PrintTemplate> PrintTemplate { get; set; }

    public WorkflowDbContext(DbContextOptions<WorkflowDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.ConfigureForm();
        builder.ConfigureFlow();
        builder.ConfigureBook();
        builder.ConfigurePrintTemplate();

        //builder.Entity<YourEntity>(b =>
        //{
        //    b.ToTable(WorkflowConsts.DbTablePrefix + "YourEntities", WorkflowConsts.DbSchema);
        //    b.ConfigureByConvention(); //auto configure for the base class props
        //    //...
        //});
    }
}
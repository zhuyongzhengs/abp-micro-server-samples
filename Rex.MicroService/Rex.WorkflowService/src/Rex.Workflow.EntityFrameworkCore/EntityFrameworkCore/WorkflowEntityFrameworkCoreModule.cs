﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AuditLogging;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.MySQL;
using Volo.Abp.FeatureManagement;
using Volo.Abp.FeatureManagement.EntityFrameworkCore;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.SettingManagement;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TenantManagement;
using Volo.Abp.TenantManagement.EntityFrameworkCore;
using XCZ.EntityFrameworkCore;

namespace Rex.Workflow.EntityFrameworkCore;

[DependsOn(
    typeof(WorkflowDomainModule),
    typeof(AbpPermissionManagementEntityFrameworkCoreModule),
    typeof(AbpSettingManagementEntityFrameworkCoreModule),
    typeof(AbpEntityFrameworkCoreMySQLModule),
    typeof(AbpAuditLoggingEntityFrameworkCoreModule),
    typeof(AbpTenantManagementEntityFrameworkCoreModule),
    typeof(AbpFeatureManagementEntityFrameworkCoreModule),
    typeof(FormEntityFrameworkCoreModule),
    typeof(FlowEntityFrameworkCoreModule)
    )]
public class WorkflowEntityFrameworkCoreModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        WorkflowEfCoreEntityExtensionMappings.Configure();
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        ChangeDbTablePrefix();
        context.Services.AddAbpDbContext<WorkflowDbContext>(options =>
        {
            /* Remove "includeAllEntities: true" to create
             * default repositories only for aggregate roots */
            options.AddDefaultRepositories(includeAllEntities: true);
        });

        Configure<AbpDbContextOptions>(options =>
        {
            /* The main point to change your DBMS.
             * See also WorkflowMigrationsDbContextFactory for EF Core tooling. */
            options.UseMySQL();
        });
    }

    /// <summary>
    /// 更改[数据库]表前缀
    /// </summary>
    private void ChangeDbTablePrefix()
    {
        #region TenantManagement

        AbpTenantManagementDbProperties.DbTablePrefix = WorkflowConsts.SysDbTablePrefix;
        AbpTenantManagementDbProperties.DbSchema = WorkflowConsts.SysDbSchema;

        #endregion TenantManagement

        #region PermissionManagement

        AbpPermissionManagementDbProperties.DbTablePrefix = WorkflowConsts.SysDbTablePrefix;
        AbpPermissionManagementDbProperties.DbSchema = WorkflowConsts.SysDbSchema;

        #endregion PermissionManagement

        #region SettingManagement

        AbpSettingManagementDbProperties.DbTablePrefix = WorkflowConsts.SysDbTablePrefix;
        AbpSettingManagementDbProperties.DbSchema = WorkflowConsts.SysDbSchema;

        #endregion SettingManagement

        #region FeatureManagement

        AbpFeatureManagementDbProperties.DbTablePrefix = WorkflowConsts.SysDbTablePrefix;
        AbpFeatureManagementDbProperties.DbSchema = WorkflowConsts.SysDbSchema;

        #endregion FeatureManagement

        #region AuditLogging

        AbpAuditLoggingDbProperties.DbTablePrefix = WorkflowConsts.SysDbTablePrefix;
        AbpAuditLoggingDbProperties.DbSchema = WorkflowConsts.SysDbSchema;

        #endregion AuditLogging
    }
}
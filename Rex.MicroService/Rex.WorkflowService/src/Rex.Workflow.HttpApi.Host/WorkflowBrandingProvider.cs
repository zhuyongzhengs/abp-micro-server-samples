﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace Rex.Workflow;

[Dependency(ReplaceServices = true)]
public class WorkflowBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "Workflow";
}

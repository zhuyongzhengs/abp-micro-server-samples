using AutoMapper;
using Rex.Workflow.PrintTemplates.Dto;
using Rex.Workflow.PrintTemplates;

namespace Rex.Workflow.PrintTemplates
{
    public class PrintTemplateAutoMapperProfile : Profile
    {
        public PrintTemplateAutoMapperProfile()
        {
            CreateMap<PrintTemplate, PrintTemplateDto>();
            CreateMap<CreateOrUpdatePrintTemplateDto, PrintTemplate>();
        }
    }
}
using AutoMapper;
using Rex.Workflow.Books.Dto;
using Rex.Workflow.Books;

namespace Rex.Workflow.Books
{
    public class BookAutoMapperProfile : Profile
    {
        public BookAutoMapperProfile()
        {
            CreateMap<Book, BookDto>();
            CreateMap<CreateOrUpdateBookDto, Book>();
        }
    }
}
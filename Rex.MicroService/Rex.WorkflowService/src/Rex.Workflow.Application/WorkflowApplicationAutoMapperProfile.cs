﻿using AutoMapper;

namespace Rex.Workflow;

public class WorkflowApplicationAutoMapperProfile : Profile
{
    public WorkflowApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */
    }
}

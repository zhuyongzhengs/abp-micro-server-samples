﻿using BaseService.HttpApi.Client;
using Volo.Abp.AutoMapper;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;
using Volo.Abp.SettingManagement;
using Volo.Abp.TenantManagement;

using XCZ;

namespace Rex.Workflow;

[DependsOn(
    typeof(WorkflowDomainModule),
    typeof(WorkflowApplicationContractsModule),
    typeof(AbpPermissionManagementApplicationModule),
    typeof(AbpTenantManagementApplicationModule),
    typeof(AbpFeatureManagementApplicationModule),
    typeof(AbpSettingManagementApplicationModule),
    typeof(BaseServiceHttpApiClientModule),
    typeof(FormApplicationModule),
    typeof(FlowApplicationModule)
    )]
public class WorkflowApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddMaps<WorkflowApplicationModule>();
        });
    }
}
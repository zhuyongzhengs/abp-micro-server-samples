﻿using Rex.Workflow.Localization;
using Volo.Abp.Application.Services;

namespace Rex.Workflow;

/* Inherit your application services from this class.
 */

public abstract class WorkflowAppService : ApplicationService
{
    protected WorkflowAppService()
    {
        LocalizationResource = typeof(WorkflowResource);
    }
}
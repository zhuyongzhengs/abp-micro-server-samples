﻿using Rex.Workflow.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace Rex.Workflow.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(WorkflowEntityFrameworkCoreModule),
    typeof(WorkflowApplicationContractsModule)
    )]
public class WorkflowDbMigratorModule : AbpModule
{

}

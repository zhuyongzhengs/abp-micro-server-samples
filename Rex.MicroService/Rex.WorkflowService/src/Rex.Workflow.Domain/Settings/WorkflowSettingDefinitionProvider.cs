﻿using Volo.Abp.Settings;

namespace Rex.Workflow.Settings;

public class WorkflowSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        //Define your own settings here. Example:
        //context.Add(new SettingDefinition(WorkflowSettings.MySetting1));
    }
}

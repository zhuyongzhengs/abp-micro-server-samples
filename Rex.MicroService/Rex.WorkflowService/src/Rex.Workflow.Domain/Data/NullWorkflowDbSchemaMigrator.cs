﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Rex.Workflow.Data;

/* This is used if database provider does't define
 * IWorkflowDbSchemaMigrator implementation.
 */
public class NullWorkflowDbSchemaMigrator : IWorkflowDbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}

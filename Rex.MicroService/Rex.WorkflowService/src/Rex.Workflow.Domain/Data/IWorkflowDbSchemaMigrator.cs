﻿using System.Threading.Tasks;

namespace Rex.Workflow.Data;

public interface IWorkflowDbSchemaMigrator
{
    Task MigrateAsync();
}

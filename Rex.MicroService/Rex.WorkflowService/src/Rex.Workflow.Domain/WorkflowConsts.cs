﻿namespace Rex.Workflow;

public static class WorkflowConsts
{
    #region 默认(Workflow)前缀

    public const string DefaultDbTablePrefix = "Wf_";
    public const string DefaultDbSchema = null;

    #endregion 默认(Workflow)前缀

    #region 系统前缀

    public const string SysDbTablePrefix = "Sys_";
    public const string SysDbSchema = null;

    #endregion 系统前缀

    #region 连接字符串

    public const string ConnectionStringName = "Workflow";

    #endregion 连接字符串
}
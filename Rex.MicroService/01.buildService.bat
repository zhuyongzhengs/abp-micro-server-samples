@echo off
:: 代码页更改为
chcp 65001

:: cmd标题名称
title ABP.MicroServer 生成项目依赖

:: 认证授权服务
@echo.
@echo 生成【认证授权服务】
start cmd /k "cd ./Rex.AuthServer/src/Rex.AuthServer.HttpApi.Host && dotnet build"

:: Base服务
@echo.
@echo 生成【Base服务】
start cmd /k "cd ./Rex.BaseService/src/Rex.BaseService.HttpApi.Host && dotnet build"

:: 工作流服务
@echo.
@echo 生成【工作流服务】
start cmd /k "cd ./Rex.WorkflowService/src/Rex.Workflow.HttpApi.Host && dotnet build"

:: 网关服务
@echo.
@echo 生成【网关服务】
start cmd /k "cd ./Rex.Gateways/Rex.GatewayService/Rex.GatewayService && dotnet build"


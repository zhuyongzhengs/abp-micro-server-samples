@echo off
:: 代码页更改为
chcp 65001

:: cmd标题名称
title ABP.MicroServer服务启动

:: 认证授权服务
@echo.
@echo 启动【认证授权服务】
start cmd /k "cd ./Rex.AuthServer/src/Rex.AuthServer.HttpApi.Host && dotnet run"

:: Base服务
@echo.
@echo 启动【Base服务】
start cmd /k "cd ./Rex.BaseService/src/Rex.BaseService.HttpApi.Host && dotnet run"

:: 工作流服务
@echo.
@echo 启动【工作流服务】
start cmd /k "cd ./Rex.WorkflowService/src/Rex.Workflow.HttpApi.Host && dotnet run"

:: 网关服务
@echo.
@echo 启动【网关服务】
start cmd /k "cd ./Rex.Gateways/Rex.GatewayService/Rex.GatewayService && dotnet run"


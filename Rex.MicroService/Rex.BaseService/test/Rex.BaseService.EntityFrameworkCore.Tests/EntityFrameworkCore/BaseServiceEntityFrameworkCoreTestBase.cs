﻿using Volo.Abp;

namespace Rex.BaseService.EntityFrameworkCore;

public abstract class BaseServiceEntityFrameworkCoreTestBase : BaseServiceTestBase<BaseServiceEntityFrameworkCoreTestModule>
{

}

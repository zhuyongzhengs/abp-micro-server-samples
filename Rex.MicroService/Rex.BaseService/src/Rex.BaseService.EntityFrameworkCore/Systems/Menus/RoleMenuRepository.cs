﻿using Microsoft.Extensions.DependencyInjection;
using Rex.BaseService.EntityFrameworkCore;
using Rex.BaseService.Systems.RoleMenus;
using System;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Rex.BaseService.Systems.Menus
{
    /// <summary>
    /// 菜单角色仓储
    /// </summary>
    [Dependency(ServiceLifetime.Singleton)]
    public class RoleMenuRepository : EfCoreRepository<BaseServiceDbContext, RoleMenu, Guid>, IRoleMenuRepository
    {
        public RoleMenuRepository(IDbContextProvider<BaseServiceDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
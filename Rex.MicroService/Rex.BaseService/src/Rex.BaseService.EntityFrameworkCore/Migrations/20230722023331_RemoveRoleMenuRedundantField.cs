﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Rex.BaseService.Migrations
{
    /// <inheritdoc />
    public partial class RemoveRoleMenuRedundantField : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Base_RoleMenus");

            migrationBuilder.DropColumn(
                name: "CreatorId",
                table: "Base_RoleMenus");

            migrationBuilder.DropColumn(
                name: "DeleterId",
                table: "Base_RoleMenus");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Base_RoleMenus");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Base_RoleMenus");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Base_RoleMenus");

            migrationBuilder.DropColumn(
                name: "LastModifierId",
                table: "Base_RoleMenus");

            migrationBuilder.AlterColumn<Guid>(
                name: "PId",
                table: "Base_Menus",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci",
                oldClrType: typeof(string),
                oldType: "longtext")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<int>(
                name: "MenuType",
                table: "Base_Menus",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .OldAnnotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Base_RoleMenus",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "CreatorId",
                table: "Base_RoleMenus",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<Guid>(
                name: "DeleterId",
                table: "Base_RoleMenus",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Base_RoleMenus",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Base_RoleMenus",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Base_RoleMenus",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "LastModifierId",
                table: "Base_RoleMenus",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.UpdateData(
                table: "Base_Menus",
                keyColumn: "PId",
                keyValue: null,
                column: "PId",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "PId",
                table: "Base_Menus",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "char(36)",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("Relational:Collation", "ascii_general_ci");

            migrationBuilder.UpdateData(
                table: "Base_Menus",
                keyColumn: "MenuType",
                keyValue: null,
                column: "MenuType",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "MenuType",
                table: "Base_Menus",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}

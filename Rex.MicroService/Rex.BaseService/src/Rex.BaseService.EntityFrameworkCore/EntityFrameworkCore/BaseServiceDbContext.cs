﻿using Microsoft.EntityFrameworkCore;
using Rex.BaseService.Systems.Menus;
using Rex.BaseService.Systems.RoleMenus;
using Volo.Abp.AuditLogging;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.FeatureManagement;
using Volo.Abp.FeatureManagement.EntityFrameworkCore;
using Volo.Abp.Identity;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.PermissionManagement;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.SettingManagement;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TenantManagement;
using Volo.Abp.TenantManagement.EntityFrameworkCore;

namespace Rex.BaseService.EntityFrameworkCore;

[ReplaceDbContext(typeof(IIdentityDbContext))]
[ReplaceDbContext(typeof(IPermissionManagementDbContext))]
[ReplaceDbContext(typeof(ITenantManagementDbContext))]
[ReplaceDbContext(typeof(ISettingManagementDbContext))]
[ReplaceDbContext(typeof(IAuditLoggingDbContext))]
[ReplaceDbContext(typeof(IFeatureManagementDbContext))]
[ConnectionStringName(BaseServiceConsts.ConnectionStringName)]
public class BaseServiceDbContext :
    AbpDbContext<BaseServiceDbContext>,
    IIdentityDbContext,
    IPermissionManagementDbContext,
    ITenantManagementDbContext,
    ISettingManagementDbContext,
    IAuditLoggingDbContext,
    IFeatureManagementDbContext
{
    /* Add DbSet properties for your Aggregate Roots / Entities here. */

    #region 租户管理

    public DbSet<Tenant> Tenants { get; set; }

    public DbSet<TenantConnectionString> TenantConnectionStrings { get; set; }

    #endregion 租户管理

    #region 用户身份

    public DbSet<IdentityUser> Users { get; set; }
    public DbSet<IdentityRole> Roles { get; set; }
    public DbSet<IdentityClaimType> ClaimTypes { get; set; }
    public DbSet<OrganizationUnit> OrganizationUnits { get; set; }
    public DbSet<IdentitySecurityLog> SecurityLogs { get; set; }
    public DbSet<IdentityLinkUser> LinkUsers { get; set; }
    public DbSet<IdentityUserDelegation> UserDelegations { get; set; }

    #endregion 用户身份

    #region 权限授予

    public DbSet<PermissionGrant> PermissionGrants { get; set; }
    public DbSet<PermissionGroupDefinitionRecord> PermissionGroups { get; set; }
    public DbSet<PermissionDefinitionRecord> Permissions { get; set; }

    #endregion 权限授予

    #region 设置管理

    public DbSet<Setting> Settings { get; set; }

    #endregion 设置管理

    #region 特征管理

    public DbSet<FeatureGroupDefinitionRecord> FeatureGroups { get; set; }

    public DbSet<FeatureDefinitionRecord> Features { get; set; }

    public DbSet<FeatureValue> FeatureValues { get; set; }

    #endregion 特征管理

    #region 审计日志

    public DbSet<AuditLog> AuditLogs { get; set; }

    #endregion 审计日志

    #region 菜单管理

    public DbSet<Menu> Menus { get; set; }
    public DbSet<RoleMenu> RoleMenus { get; set; }

    #endregion 菜单管理

    public BaseServiceDbContext(DbContextOptions<BaseServiceDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        /* Include modules to your migration db context */

        #region 租户管理

        AbpTenantManagementDbProperties.DbTablePrefix = BaseServiceConsts.SysDbTablePrefix;
        AbpTenantManagementDbProperties.DbSchema = BaseServiceConsts.SysDbSchema;
        builder.ConfigureTenantManagement();

        #endregion 租户管理

        #region 身份(用户)

        AbpIdentityDbProperties.DbTablePrefix = BaseServiceConsts.SysDbTablePrefix;
        AbpIdentityDbProperties.DbSchema = BaseServiceConsts.SysDbSchema;
        builder.ConfigureIdentity();

        #endregion 身份(用户)

        #region 权限管理

        AbpPermissionManagementDbProperties.DbTablePrefix = BaseServiceConsts.SysDbTablePrefix;
        AbpPermissionManagementDbProperties.DbSchema = BaseServiceConsts.SysDbSchema;
        builder.ConfigurePermissionManagement();

        #endregion 权限管理

        #region 设置管理

        AbpSettingManagementDbProperties.DbTablePrefix = BaseServiceConsts.SysDbTablePrefix;
        AbpSettingManagementDbProperties.DbSchema = BaseServiceConsts.SysDbSchema;
        builder.ConfigureSettingManagement();

        #endregion 设置管理

        #region 特征管理

        AbpFeatureManagementDbProperties.DbTablePrefix = BaseServiceConsts.SysDbTablePrefix;
        AbpFeatureManagementDbProperties.DbSchema = BaseServiceConsts.SysDbSchema;
        builder.ConfigureFeatureManagement();

        #endregion 特征管理

        #region 审计日志

        AbpAuditLoggingDbProperties.DbTablePrefix = BaseServiceConsts.SysDbTablePrefix;
        AbpAuditLoggingDbProperties.DbSchema = BaseServiceConsts.SysDbSchema;
        builder.ConfigureAuditLogging();

        #endregion 审计日志

        #region 菜单管理

        builder.ConfigureMenuManagement();

        #endregion 菜单管理

        /* Configure your own tables/entities inside here */
        //builder.Entity<YourEntity>(b =>
        //{
        //    b.ToTable(BaseServiceConsts.DbTablePrefix + "YourEntities", BaseServiceConsts.DbSchema);
        //    b.ConfigureByConvention(); //auto configure for the base class props
        //    //...
        //});
    }
}
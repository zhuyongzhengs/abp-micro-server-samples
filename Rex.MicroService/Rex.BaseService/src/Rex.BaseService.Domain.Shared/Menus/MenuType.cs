﻿namespace Rex.BaseService.Menus
{
    /// <summary>
    /// 菜单类型
    /// </summary>
    public enum MenuType
    {
        /// <summary>
        /// 菜单
        /// </summary>
        Menu = 1,

        /// <summary>
        /// 按钮
        /// </summary>
        Button = 2
    }
}
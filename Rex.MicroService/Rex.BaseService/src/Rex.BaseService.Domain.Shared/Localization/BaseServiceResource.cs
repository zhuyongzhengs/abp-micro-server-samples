﻿using Volo.Abp.Localization;

namespace Rex.BaseService.Localization;

[LocalizationResourceName("BaseService")]
public class BaseServiceResource
{

}

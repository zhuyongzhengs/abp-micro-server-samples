﻿using Rex.BaseService.Localization;
using Rex.Service.Core.Permissions.BaseServices;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace Rex.BaseService.Permissions;

public class BaseServicePermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var baseGroup = context.AddGroup(BaseServicePermissions.GroupName);

        #region 首页定义

        context.AddBaseHomePermissionDefine(baseGroup);

        #endregion 首页定义

        #region 菜单定义

        context.AddBaseMenuPermissionDefine(baseGroup);

        #endregion 菜单定义

        #region 角色菜单定义

        context.AddBaseRoleMenuPermissionDefine(baseGroup);

        #endregion 角色菜单定义

        #region 组织单元定义

        context.AddBaseOrganizationUnitDefine(baseGroup);

        #endregion 组织单元定义

        #region 审计日志定义

        context.AddBaseAuditLoggingPermissionDefine(baseGroup);

        #endregion 审计日志定义

        #region 安全日志定义

        context.AddBaseSecurityLogPermissionDefine(baseGroup);

        #endregion 安全日志定义
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<BaseServiceResource>(name);
    }
}
﻿using System;
using System.Collections.Generic;

namespace Rex.BaseService.Systems.AuditLoggings
{
    /// <summary>
    /// 每天的平均执行时间Dto
    /// </summary>
    public class AverageExecutionDurationPerDayDto
    {
        /// <summary>
        /// 数据
        /// </summary>
        public Dictionary<DateTime, double> Data { get; set; }
    }
}
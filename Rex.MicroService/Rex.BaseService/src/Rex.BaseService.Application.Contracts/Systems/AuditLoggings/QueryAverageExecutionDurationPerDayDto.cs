﻿using System;

namespace Rex.BaseService.Systems.AuditLoggings
{
    /// <summary>
    /// 查询每天的平均执行时间Dto
    /// </summary>
    public class QueryAverageExecutionDurationPerDayDto
    {
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndDate { get; set; }
    }
}
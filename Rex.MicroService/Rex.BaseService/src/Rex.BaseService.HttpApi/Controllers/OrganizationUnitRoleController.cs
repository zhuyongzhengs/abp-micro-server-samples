﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rex.BaseService.Systems.OrganizationUnitRoles;
using Rex.Service.Core.Permissions.BaseServices;
using Rex.Service.Core.Results;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rex.BaseService.Controllers
{
    /// <summary>
    /// 角色菜单控制器
    /// </summary>
    [Route("api/base/organization-unit-role")]
    [Authorize(BaseServicePermissions.OrganizationUnits.ManagingRole)]
    [BmResult]
    public class OrganizationUnitRoleController : BaseServiceController
    {
        private readonly IOrganizationUnitRoleAppService _organizationUnitRoleAppService;
        private readonly ILogger<OrganizationUnitRoleController> _logger;

        public OrganizationUnitRoleController(IOrganizationUnitRoleAppService organizationUnitRoleAppService, ILogger<OrganizationUnitRoleController> logger)
        {
            _organizationUnitRoleAppService = organizationUnitRoleAppService;
            _logger = logger;
        }

        /// <summary>
        /// 删除组织单元【角色】信息
        /// </summary>
        /// <param name="organizationUnitId">组织单位ID</param>
        /// <param name="roleIds">角色ID</param>
        /// <returns></returns>
        [HttpDelete("{organizationUnitId}")]
        public async Task DeleteByOurIdAsync(Guid organizationUnitId, [FromBody] List<Guid> roleIds)
        {
            await _organizationUnitRoleAppService.DeleteByOuIdAsync(organizationUnitId, roleIds);
        }
    }
}
﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rex.BaseService.Systems.UserOrganizationUnits;
using Rex.Service.Core.Permissions.BaseServices;
using Rex.Service.Core.Results;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rex.BaseService.Controllers
{
    /// <summary>
    /// 角色菜单控制器
    /// </summary>
    [Route("api/base/user-organization-unit")]
    [Authorize(BaseServicePermissions.OrganizationUnits.ManagingUser)]
    [BmResult]
    public class UserOrganizationUnitController : BaseServiceController
    {
        private readonly IUserOrganizationUnitAppService _userOrganizationUnitAppService;
        private readonly ILogger<UserOrganizationUnitController> _logger;

        public UserOrganizationUnitController(IUserOrganizationUnitAppService userOrganizationUnitAppService, ILogger<UserOrganizationUnitController> logger)
        {
            _userOrganizationUnitAppService = userOrganizationUnitAppService;
            _logger = logger;
        }

        /// <summary>
        /// 删除组织单元【用户】信息
        /// </summary>
        /// <param name="organizationUnitId">组织单位ID</param>
        /// <param name="userIds">用户ID</param>
        /// <returns></returns>
        [HttpDelete("{organizationUnitId}")]
        public async Task DeleteByOuIdAsync(Guid organizationUnitId, [FromBody] List<Guid> userIds)
        {
            await _userOrganizationUnitAppService.DeleteByOuIdAsync(organizationUnitId, userIds);
        }
    }
}
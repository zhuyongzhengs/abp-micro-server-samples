﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rex.BaseService.Systems.RoleMenus;

namespace Rex.BaseService.Controllers
{
    /// <summary>
    /// 角色菜单控制器
    /// </summary>
    [Route("api/rolemenu")]
    public class RoleMenuController : BaseServiceController
    {
        private readonly IRoleMenuAppService _menuAppService;
        private readonly ILogger<RoleMenuController> _logger;

        public RoleMenuController(IRoleMenuAppService commodityAppService, ILogger<RoleMenuController> logger)
        {
            _menuAppService = commodityAppService;
            _logger = logger;
        }

        /*
        /// <summary>
        /// 获取当前(角色)树形菜单
        /// </summary>
        /// <returns></returns>
        [HttpGet("tree")]
        [Authorize(BaseServicePermissions.RoleMenus.Select)]
        public async Task<List<MenuTreeDto>> GetTree()
        {
            return await _menuAppService.GetTree();
        }
        */
    }
}
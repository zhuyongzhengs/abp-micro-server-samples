﻿using AutoMapper;
using Rex.BaseService.Systems.AuditLoggings;
using Rex.BaseService.Systems.Menus;
using Rex.BaseService.Systems.OrganizationUnitRoles;
using Rex.BaseService.Systems.OrganizationUnits;
using Rex.BaseService.Systems.RoleMenus;
using Rex.BaseService.Systems.SecurityLogs;
using Rex.BaseService.Systems.UserOrganizationUnits;
using Volo.Abp.AuditLogging;
using Volo.Abp.Identity;

namespace Rex.BaseService;

public class BaseServiceApplicationAutoMapperProfile : Profile
{
    public BaseServiceApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */

        #region Menus

        CreateMap<CreateMenuDto, Menu>();
        CreateMap<UpdateMenuDto, Menu>();
        CreateMap<MenuDto, Menu>();
        CreateMap<Menu, MenuDto>();
        CreateMap<Menu, MenuTreeDto>();

        #endregion Menus

        #region RoleMenus

        CreateMap<CreateRoleMenuDto, RoleMenu>();
        CreateMap<UpdateRoleMenuDto, RoleMenu>();
        CreateMap<RoleMenu, RoleMenuDto>();

        #endregion RoleMenus

        #region OrganizationUnits

        CreateMap<CreateOrganizationUnitDto, OrganizationUnit>();
        CreateMap<UpdateOrganizationUnitDto, OrganizationUnit>();
        CreateMap<OrganizationUnit, OrganizationUnitDto>();

        CreateMap<OrganizationUnit, OrganizationUnitTreeDto>();
        CreateMap<OrganizationUnitTreeDto, OrganizationUnit>();

        #endregion OrganizationUnits

        #region UserOrganizationUnits

        CreateMap<CreateUserOrganizationUnitDto, IdentityUserOrganizationUnit>();
        CreateMap<UpdateUserOrganizationUnitDto, IdentityUserOrganizationUnit>();
        CreateMap<IdentityUserOrganizationUnit, UserOrganizationUnitDto>();

        #endregion UserOrganizationUnits

        #region OrganizationUnitRoles

        CreateMap<CreateOrganizationUnitRoleDto, OrganizationUnitRole>();
        CreateMap<UpdateOrganizationUnitRoleDto, OrganizationUnitRole>();
        CreateMap<OrganizationUnitRole, OrganizationUnitRoleDto>();

        #endregion OrganizationUnitRoles

        #region AuditLogs

        CreateMap<AuditLog, AuditLogDto>()
                .ForMember(t => t.EntityChanges, option => option.MapFrom(l => l.EntityChanges))
                .ForMember(t => t.Actions, option => option.MapFrom(l => l.Actions));
        CreateMap<EntityChange, EntityChangeDto>()
             .ForMember(t => t.PropertyChanges, option => option.MapFrom(l => l.PropertyChanges));

        CreateMap<AuditLogAction, AuditLogActionDto>();
        CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();

        #endregion AuditLogs

        #region SecurityLogs

        CreateMap<IdentitySecurityLog, SecurityLogDto>();
        CreateMap<SecurityLogDto, IdentitySecurityLog>();

        #endregion SecurityLogs
    }
}
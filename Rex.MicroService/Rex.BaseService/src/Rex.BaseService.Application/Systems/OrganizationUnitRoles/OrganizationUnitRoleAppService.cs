﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Rex.Service.Core.Permissions.BaseServices;
using Rex.Service.Core.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;

namespace Rex.BaseService.Systems.OrganizationUnitRoles
{
    /// <summary>
    /// 组织单元【角色】服务
    /// </summary>
    [Dependency(ServiceLifetime.Singleton)]
    [Authorize(BaseServicePermissions.OrganizationUnits.ManagingRole)]
    [BmResult]
    public class OrganizationUnitRoleAppService : ApplicationService, IOrganizationUnitRoleAppService
    {
        public IRepository<OrganizationUnitRole> OrganizationUnitRoleRepository { get; set; }
        public IIdentityRoleRepository IdentityRoleRepository { get; set; }
        private readonly IOrganizationUnitRoleRepository _roleOrganizationUnitRepository;

        public OrganizationUnitRoleAppService(IOrganizationUnitRoleRepository roleOrganizationUnitRepository)
        {
            _roleOrganizationUnitRepository = roleOrganizationUnitRepository;
        }

        /// <summary>
        /// 获取组织单元【角色】信息
        /// </summary>
        /// <param name="filter">过滤筛选</param>
        /// <param name="maxResultCount">查询数量</param>
        /// <param name="skipCount">跳过数</param>
        /// <param name="organizationUnitId">组织单元ID</param>
        /// <param name="sorting">排序</param>
        /// <returns></returns>
        public async Task<PagedResultDto<OrganizationUnitRoleDto>> GetListAsync(string? filter, int maxResultCount, int skipCount, Guid? organizationUnitId = null, string sorting = null)
        {
            long totalCount = await _roleOrganizationUnitRepository.GetListCountAsync(filter, organizationUnitId);
            if (totalCount < 1) return new PagedResultDto<OrganizationUnitRoleDto>();

            List<OrganizationUnitRole> roleOrganizationUnitList = await _roleOrganizationUnitRepository.GetListAsync(filter, maxResultCount, skipCount, organizationUnitId, sorting);
            List<Guid> roleIds = roleOrganizationUnitList.Select(p => p.RoleId).ToList();
            List<IdentityRole> identityRoles = await IdentityRoleRepository.GetListAsync(roleIds);

            List<OrganizationUnitRoleDto> roleOuList = ObjectMapper.Map<List<OrganizationUnitRole>, List<OrganizationUnitRoleDto>>(roleOrganizationUnitList);
            foreach (var roleOu in roleOuList)
            {
                roleOu.RoleName = identityRoles.Where(u => u.Id == roleOu.RoleId).FirstOrDefault().Name;
            }
            return new PagedResultDto<OrganizationUnitRoleDto>(totalCount, roleOuList); ;
        }

        /// <summary>
        /// 创建组织单元【角色】信息
        /// </summary>
        /// <param name="input">角色组织单元</param>
        /// <returns></returns>
        public async Task CreateAsync(CreateOrganizationUnitRoleDto input)
        {
            OrganizationUnitRole roleOrganizationUnit = new OrganizationUnitRole(input.RoleId, input.OrganizationUnitId, CurrentTenant.Id);
            if (roleOrganizationUnit != null) await OrganizationUnitRoleRepository.InsertAsync(roleOrganizationUnit);
        }

        /// <summary>
        /// 批量创建组织单元【角色】信息
        /// </summary>
        /// <param name="input">角色组织单元</param>
        /// <returns></returns>
        public async Task CreateManyAsync(List<CreateOrganizationUnitRoleDto> input)
        {
            List<OrganizationUnitRole> roleOrganizationUnitList = new List<OrganizationUnitRole>();
            foreach (var ourItem in input)
            {
                roleOrganizationUnitList.Add(new OrganizationUnitRole(ourItem.RoleId, ourItem.OrganizationUnitId, CurrentTenant.Id));
            }
            if (roleOrganizationUnitList.Count > 0) await OrganizationUnitRoleRepository.InsertManyAsync(roleOrganizationUnitList);
        }

        /// <summary>
        /// 删除组织单元【角色】信息
        /// </summary>
        /// <param name="organizationUnitId">组织单位ID</param>
        /// <param name="roleIds">角色ID</param>
        /// <returns></returns>
        [RemoteService(false)]
        public async Task DeleteByOuIdAsync(Guid organizationUnitId, List<Guid> roleIds)
        {
            await OrganizationUnitRoleRepository.DeleteAsync(p => p.OrganizationUnitId == organizationUnitId && roleIds.Contains(p.RoleId));
        }

        /// <summary>
        /// 选择组织单元角色
        /// </summary>
        /// <returns></returns>
        public async Task<PagedResultDto<IdentityRoleDto>> GetSelectRoleListAsync(Guid? organizationUnitId = null, string sorting = null, int maxResultCount = int.MaxValue, int skipCount = 0, string filter = null, bool includeDetails = false)
        {
            List<Guid> noRoleIds = new List<Guid>();
            if (organizationUnitId.HasValue)
            {
                List<OrganizationUnitRole> organizationUnitRoleList = await _roleOrganizationUnitRepository.GetListAsync(p => p.OrganizationUnitId == organizationUnitId.Value);
                if (organizationUnitRoleList.Count > 0)
                {
                    noRoleIds = organizationUnitRoleList.Select(p => p.RoleId).ToList();
                }
            }
            long totalCount = await _roleOrganizationUnitRepository.GetSelectRoleCountAsync(noRoleIds, filter);
            if (totalCount < 1) return new PagedResultDto<IdentityRoleDto>();

            List<IdentityRole> identityRoleList = await _roleOrganizationUnitRepository.GetSelectRoleListAsync(noRoleIds, sorting, maxResultCount, skipCount, filter, includeDetails);
            List<IdentityRoleDto> roleList = ObjectMapper.Map<List<IdentityRole>, List<IdentityRoleDto>>(identityRoleList);
            return new PagedResultDto<IdentityRoleDto>(totalCount, roleList);
        }
    }
}
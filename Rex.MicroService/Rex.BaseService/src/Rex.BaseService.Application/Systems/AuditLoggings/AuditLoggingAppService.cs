﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Rex.Service.Core.Permissions.BaseServices;
using Rex.Service.Core.Results;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.AuditLogging;
using Volo.Abp.DependencyInjection;

namespace Rex.BaseService.Systems.AuditLoggings
{
    /// <summary>
    /// 审计日志服务
    /// </summary>
    [Dependency(ServiceLifetime.Singleton)]
    [Authorize(BaseServicePermissions.AuditLoggings.Default)]
    [BmResult]
    public class AuditLoggingAppService : ApplicationService, IAuditLoggingAppService
    {
        private readonly IAuditLogRepository _auditLogRepository;

        public AuditLoggingAppService(
            IAuditLogRepository auditLogRepository)
        {
            _auditLogRepository = auditLogRepository;
        }

        /// <summary>
        /// 根据ID获取审计日志
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        public async Task<AuditLogDto> GetAsync(Guid id)
        {
            var auditLog = await _auditLogRepository.GetAsync(id);
            return ObjectMapper.Map<AuditLog, AuditLogDto>(auditLog);
        }

        /// <summary>
        /// 查询审计日志
        /// </summary>
        /// <param name="input">查询条件</param>
        /// <returns></returns>
        public async Task<PagedResultDto<AuditLogDto>> GetAllAsync(QueryAuditLogDto input)
        {
            // 获取数量
            long totalCount = await _auditLogRepository.GetCountAsync(httpMethod: input.HttpMethod, url: input.Url,
                userName: input.UserName, applicationName: input.ApplicationName, correlationId: input.CorrelationId, maxExecutionDuration: input.MaxExecutionDuration,
                minExecutionDuration: input.MinExecutionDuration, hasException: input.HasException, httpStatusCode: input.HttpStatusCode, startTime: input.BeginTime, endTime: input.EndTime);

            // 获取审计日志数据
            List<AuditLog> auditLogDtoList = await _auditLogRepository.GetListAsync(sorting: input.Sorting, maxResultCount: input.MaxResultCount, skipCount: input.SkipCount, httpMethod: input.HttpMethod, url: input.Url,
                userName: input.UserName, applicationName: input.ApplicationName, correlationId: input.CorrelationId, maxExecutionDuration: input.MaxExecutionDuration,
                minExecutionDuration: input.MinExecutionDuration, hasException: input.HasException, httpStatusCode: input.HttpStatusCode, startTime: input.BeginTime, endTime: input.EndTime, includeDetails: true);

            return new PagedResultDto<AuditLogDto>(
                totalCount,
                ObjectMapper.Map<List<AuditLog>, List<AuditLogDto>>(auditLogDtoList)
            );
        }

        /// <summary>
        /// 获取每天的平均执行时间
        /// </summary>
        /// <param name="input">查询条件</param>
        /// <returns></returns>
        public async Task<AverageExecutionDurationPerDayDto> GetAverageExecutionDurationPerDayAsync(QueryAverageExecutionDurationPerDayDto input)
        {
            var data = await _auditLogRepository.GetAverageExecutionDurationPerDayAsync(input.StartDate, input.EndDate);
            return new AverageExecutionDurationPerDayDto()
            {
                Data = data
            };
        }
    }
}
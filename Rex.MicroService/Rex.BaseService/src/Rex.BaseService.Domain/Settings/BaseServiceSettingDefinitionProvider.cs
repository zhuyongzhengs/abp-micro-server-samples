﻿using Volo.Abp.Settings;

namespace Rex.BaseService.Settings;

public class BaseServiceSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        //Define your own settings here. Example:
        //context.Add(new SettingDefinition(BaseServiceSettings.MySetting1));
    }
}

import Cookies from 'js-cookie';

/**
 * window.localStorage 浏览器永久缓存
 * @method set 设置永久缓存
 * @method get 获取永久缓存
 * @method remove 移除永久缓存
 * @method clear 移除全部永久缓存
 */
export const Local = {
	// 查看 v2.4.3版本更新日志
	setKey(key: string) {
		// @ts-ignore
		return `${__NEXT_NAME__}:${key}`;
	},
	// 设置永久缓存
	set<T>(key: string, val: T) {
		window.localStorage.setItem(Local.setKey(key), JSON.stringify(val));
	},
	// 获取永久缓存
	get(key: string) {
		let json = <string>window.localStorage.getItem(Local.setKey(key));
		return JSON.parse(json);
	},
	// 移除永久缓存
	remove(key: string) {
		window.localStorage.removeItem(Local.setKey(key));
	},
	// 移除全部永久缓存
	clear() {
		window.localStorage.clear();
	}
};

/**
 * window.sessionStorage 浏览器临时缓存
 * @method set 设置临时缓存
 * @method get 获取临时缓存
 * @method remove 移除临时缓存
 * @method clear 移除全部临时缓存
 */
export const Session = {
	// 设置临时缓存
	set<T>(key: string, val: T) {
		if (key === 'token') return Cookies.set(key, val);
		window.sessionStorage.setItem(Local.setKey(key), JSON.stringify(val));
	},
	// 获取临时缓存
	get(key: string) {
		if (key === 'token') return Cookies.get(key);
		let json = <string>window.sessionStorage.getItem(Local.setKey(key));
		return JSON.parse(json);
	},
	// 移除临时缓存
	remove(key: string) {
		if (key === 'token') return Cookies.remove(key);
		window.sessionStorage.removeItem(Local.setKey(key));
	},
	// 移除全部临时缓存
	clear() {
		Cookies.remove('token');
		Cookies.remove('refresh_token');
		window.sessionStorage.clear();
	}
};

// 默认租户Key
const defaultTenantKey = "__tenant";
const defaultTenantNameKey = "__tenantName";

/**
 * 租户切换
 */
export const TenantSwitch = {
	// 设置租户
	set(tenantId: string, tenantName: string) {
		try {
			Cookies.set(defaultTenantNameKey, tenantName);
			Cookies.set(defaultTenantKey, tenantId);
		} catch (error) {
			console.error("设置租户出错：", error);
			return false;
		}
		return true;
	},
	// 获取租户ID Key
	getTenantIdKey() {
		return defaultTenantKey;
	},
	// 获取租户ID
	getTenantId() {
		return Cookies.get(defaultTenantKey);
	},
	// 获取租户名称 Key
	getTenantNameKey() {
		return defaultTenantNameKey;
	},
	// 获取租户名称
	getTenantName() {
		return Cookies.get(defaultTenantNameKey);
	},
	// 移除租户
	remove() {
		try {
			Cookies.remove(defaultTenantKey);
			Cookies.remove(defaultTenantNameKey);
		} catch (error) {
			console.error("移除租户出错：", error);
			return false;
		}
		return true;
	}
}

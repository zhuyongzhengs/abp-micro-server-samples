import axios, { AxiosInstance } from 'axios';
import { ElMessage, ElMessageBox } from 'element-plus';
import { Session, TenantSwitch } from '/@/utils/storage';
import qs from 'qs';

const baseAuthUrl: string = import.meta.env.VITE_API_AUTH_URL;
const baseUrl: string = import.meta.env.VITE_API_URL;

// 配置新建一个 axios 实例
const service: AxiosInstance = axios.create({
	baseURL: baseUrl,
	timeout: 50000,
	headers: { 'Content-Type': 'application/json' },
	paramsSerializer: {
		serialize(params) {
			return qs.stringify(params, { allowDots: true });
		}
	}
});

// 添加请求拦截器
service.interceptors.request.use(
	(config) => {
		// 在发送请求之前做些什么 token
		if (Session.get('token')) {
			config.headers!['Authorization'] = `Bearer ${Session.get('token')}`;
		}
		// 租户
		if(TenantSwitch.getTenantId()) {
			config.headers![TenantSwitch.getTenantIdKey()] = TenantSwitch.getTenantId();
		}
		// 国际化（先写死）
		config.headers!['Accept-Language'] = 'zh-Hans';
		return config;
	},
	(error) => {
		// 对请求错误做些什么
		return Promise.reject(error);
	}
);

// 添加响应拦截器
service.interceptors.response.use(
	(result: any) => {
		const resData = result.data;
		if(result.config.baseURL?.indexOf(baseAuthUrl) >= 0) {
			// 授权响应
			return resData;
		}

		// 请求响应
		if(resData.code === 0) {
			return resData.data;
		} else if(resData.code === -1) {
			console.error(resData.message);
			ElMessageBox.alert('系统错误：' + resData.message, '提示');
		} else if(resData.code === 1) {
			console.warn(resData.message);
			ElMessageBox.alert('请求失败！' + resData.message, '提示');
		} else {
			return true;
		}
		return Promise.reject(service.interceptors.response);
	},
	(error) => {
		if (error.message.indexOf('timeout') != -1) {
			ElMessage.error('网络超时');
		} else if (error.message == 'Network Error') {
			ElMessage.error('网络连接错误');
		} else if(error.response.status === 401) {
			setTimeout(() => {
				Session.clear();
				window.location.href = '/';
			}, 3 * 1000);
			ElMessageBox.alert('401：你还未登录系统(或已失效)，请重新登录！', '提示');
		} else if(error.response.status == 403) {
			ElMessage.warning('403：已禁止访问，您可能没有访问此操作的权限！');
		} else {
			let errStatus = error.response?.status;
			let errDescription = error.response?.data?.error_description;
			ElMessage.error(`${errStatus}：${errDescription}`);
		}
		return Promise.reject(error);
	}
);

// 导出 axios 实例
export default service;

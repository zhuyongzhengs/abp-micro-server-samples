import request from '/@/utils/request';

// 授权链接
const baseAuthUrl: string = import.meta.env.VITE_API_AUTH_URL;

export function useUserApi() {
    return {
        /**
         * 查询用户(分页)列表信息
         * @param { object }
         * @returns
         */
        getUserList: (params?: object) => {
			return request({
				baseURL: baseAuthUrl,
				url: '/api/identity/users',
				method: 'get',
                params
			});
		},

        /**
         * 查询用户角色信息
         * @param { object }
         * @returns
         */
        getUserRoles: (id: string) => {
			return request({
				baseURL: baseAuthUrl,
				url: `/api/identity/users/${ id }/roles`,
				method: 'get'
			});
		},

        /**
		 * 添加用户
		 * @param params 
		 * @returns 
		 */
		addUser: (data: object) => {
			return request({
                baseURL: baseAuthUrl,
				url: '/api/identity/users',
				method: 'post',
				data
			});
		},

		/**
		 * 修改用户
		 * @param params 
		 * @returns 
		 */
		updateUser: (id: string, data: object) => {
			return request({
                baseURL: baseAuthUrl,
				url: '/api/identity/users/' + id,
				method: 'put',
				data
			});
		},

		/**
		 * 删除用户
		 * @param params 
		 * @returns 
		 */
		deleteUser: (id: string) => {
			return request({
                baseURL: baseAuthUrl,
				url: '/api/identity/users/' + id,
				method: 'delete'
			});
		}
    };
}
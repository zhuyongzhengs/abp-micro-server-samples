import request from '/@/utils/request';
import { TenantSwitch } from '/@/utils/storage';

// 授权链接
const baseAuthUrl: string = import.meta.env.VITE_API_AUTH_URL;

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 登录api接口集合
 * @method signIn 用户登录
 * @method signOut 用户退出登录
 * @method getUserInfo 获取用户信息
 */
export function useLoginApi() {
	return {
		signIn: (data: object) => {
			let url = '/connect/token';
			let tId = TenantSwitch.getTenantId();
			if(tId) {
				url += "?__tenant=" + tId
			}
			return request({
				baseURL: baseAuthUrl,
				url,
				method: 'post',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data
			});
		},
		getUserInfo: () => {
			return request({
				baseURL: baseAuthUrl,
				url: '/connect/userinfo',
				method: 'get'
			});
		},
		getApplicationConfig: () => {
			return request({
				baseURL: baseAuthUrl,
				url: '/api/abp/application-configuration',
				method: 'get'
			});
		}
	};
}

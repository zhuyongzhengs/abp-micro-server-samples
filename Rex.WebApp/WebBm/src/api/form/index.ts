import request from '/@/utils/request';

export function useFormApi() {
    return {
        /**
         * 查询表单(分页)列表信息
         * @param { object }
         * @returns
         */
        getFormList: (params?: object) => {
			return request({
				url: '/api/business/form',
				method: 'get',
                params
			});
		},

        /**
		 * 添加表单
		 * @param params 
		 * @returns 
		 */
		addForm: (data: object) => {
			return request({
				url: '/api/business/form',
				method: 'post',
				data
			});
		},

		/**
		 * 修改表单
		 * @param params 
		 * @returns 
		 */
		updateForm: (id: string, data: object) => {
			return request({
				url: '/api/business/form/' + id,
				method: 'put',
				data
			});
		},

		/**
		 * 删除表单
		 * @param params 
		 * @returns 
		 */
		deleteForm: (data: string[]) => {
			return request({
				url: '/api/business/form',
				method: 'post',
				data
			});
		}
    };
}
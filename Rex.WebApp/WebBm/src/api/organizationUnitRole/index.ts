import request from '/@/utils/request';

export function useOrganizationUnitRoleApi() {
	return {
		/**
		 * 获取[角色]组织单元
		 * @returns
		 */
		getOrganizationUnitRoleList: (params?: object) => {
			return request({
				url: '/api/base/organization-unit-role',
				method: 'get',
				params
			});
		},

		/**
		 * 获取选择[角色]组织单元
		 * @returns
		 */
		getSelectOuRoleList: (organizationUnitId: string, data?: object) => {
			return request({
				url: '/api/base/organization-unit-role/select-role-list/' + organizationUnitId,
				method: 'get',
				data
			});
		},

		/**
		 * 添加[角色]组织单元
		 * @returns
		 */
		addOrganizationUnitRole: (data?: object) => {
			return request({
				url: '/api/base/organization-unit-role/many',
				method: 'post',
				data
			});
		},

		/**
		 * 删除[角色]组织单元
		 * @param ouId 
		 * @returns 
		 */
		deleteOrganizationUnitRole: (ouId: string, data: string[]) => {
			return request({
				url: '/api/base/organization-unit-role/' + ouId,
				method: 'delete',
				data
			});
		}
	};
}

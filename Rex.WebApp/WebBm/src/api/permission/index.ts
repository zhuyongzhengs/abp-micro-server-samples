import request from '/@/utils/request';

// 授权链接
const baseAuthUrl: string = import.meta.env.VITE_API_AUTH_URL;

export function usePermissionApi() {
    return {
        /**
         * 查询权限信息
         * @returns RoleList
         */
        getPermissions: (params?: object) => {
			return request({
				baseURL: baseAuthUrl,
				url: '/api/permission-management/permissions',
				method: 'get',
                params
			});
		},

		/**
		 * 修改权限信息
		 * @param params 
		 * @returns 
		 */
		updatePermissions: (params?: object, data?: object) => {
			return request({
                baseURL: baseAuthUrl,
				url: '/api/permission-management/permissions',
				method: 'put',
                params,
				data
			});
		}
    };
}
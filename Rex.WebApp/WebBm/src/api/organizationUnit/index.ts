import request from '/@/utils/request';

export function useOrganizationUnitApi() {
	return {
		/**
		 * 获取（树形）组织单位
		 * @returns
		 */
		getOrganizationUnitTree: (params?: object) => {
			return request({
				url: '/api/base/organization-unit/tree',
				method: 'get',
				params
			});
		},

		/**
		 * 添加组织单位
		 * @param params 
		 * @returns 
		 */
		addOrganizationUnit: (data: object) => {
			return request({
				url: '/api/base/organization-unit',
				method: 'post',
				data
			});
		},

		/**
		 * 修改组织单位
		 * @param params 
		 * @returns 
		 */
		updateOrganizationUnit: (data: object) => {
			return request({
				url: '/api/base/organization-unit',
				method: 'put',
				data
			});
		},

		/**
		 * 删除菜单
		 * @param params 
		 * @returns 
		 */
		deleteOrganizationUnit: (id: string) => {
			return request({
				url: '/api/base/organization-unit/' + id,
				method: 'delete'
			});
		}
	};
}

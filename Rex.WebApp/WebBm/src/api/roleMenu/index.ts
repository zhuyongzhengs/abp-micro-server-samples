import request from '/@/utils/request';

/**
 * 以下为模拟接口地址，gitee 的不通，就换自己的真实接口地址
 *
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * @method useRoleMenuApi 获取后端动态路由菜单
 */
export function useRoleMenuApi() {
	return {
		/**
		 * 根据角色ID获取角色菜单
		 * @param roleId 
		 * @returns 
		 */
		getRoleMenuByRoleId: (roleId: string) => {
			return request({
				url: '/api/base/role-menu/role-id/' + roleId,
				method: 'get'
			});
		},

		/**
		 * 批量修改角色菜单菜单
		 * @param params 
		 * @returns 
		 */
		updateManyRoleMenu: (data: object) => {
			return request({
				url: '/api/base/role-menu/many-role-menu',
				method: 'put',
				data
			});
		}
	};
}

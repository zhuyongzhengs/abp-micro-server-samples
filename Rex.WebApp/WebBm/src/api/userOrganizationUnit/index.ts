import request from '/@/utils/request';

export function useUserOrganizationUnitApi() {
	return {
		/**
		 * 获取[用户]组织单元
		 * @returns
		 */
		getUserOrganizationUnitList: (params?: object) => {
			return request({
				url: '/api/base/user-organization-unit',
				method: 'get',
				params
			});
		},

		/**
         * 选择组织单元用户(分页)列表信息
         * @param { object }
         * @returns
         */
        getSelectUserOuList: (params?: object) => {
			return request({
				url: '/api/base/user-organization-unit/select-user-list',
				method: 'get',
                params
			});
		},

		/**
		 * 添加[用户]组织单元
		 * @returns
		 */
		addUserOrganizationUnit: (data?: object) => {
			return request({
				url: '/api/base/user-organization-unit/many',
				method: 'post',
				data
			});
		},

		/**
		 * 删除[用户]组织单元
		 * @param ouId 
		 * @returns 
		 */
		deleteUserOrganizationUnit: (ouId: string, data: string[]) => {
			return request({
				url: '/api/base/user-organization-unit/' + ouId,
				method: 'delete',
				data
			});
		}
	};
}

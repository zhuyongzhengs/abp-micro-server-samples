import request from '/@/utils/request';

// 授权链接
const baseAuthUrl: string = import.meta.env.VITE_API_AUTH_URL;

export function useTenantApi() {
    return {
		/**
         * 查询租户名称信息
         * @param { object }
         * @returns
         */
        getTenantByName: (name: string) => {
			return request({
				baseURL: baseAuthUrl,
				url: '/api/abp/multi-tenancy/tenants/by-name/' + name,
				method: 'get'
			});
		},

		
		/**
         * 赋予菜单权限
         * @param { object }
         * @returns
         */
        setMenuPermissions: (tenantId: string) => {
			return request({
				url: '/api/base/menu/grant-menu-permissions/' + tenantId,
				method: 'post'
			});
		},

        /**
         * 查询租户(分页)列表信息
         * @param { object }
         * @returns
         */
        getTenantList: (params?: object) => {
			return request({
				baseURL: baseAuthUrl,
				url: '/api/multi-tenancy/tenants',
				method: 'get',
                params
			});
		},

        /**
		 * 添加租户
		 * @param params 
		 * @returns 
		 */
		addTenant: (data: object) => {
			return request({
                baseURL: baseAuthUrl,
				url: '/api/multi-tenancy/tenants',
				method: 'post',
				data
			});
		},

		/**
		 * 修改租户
		 * @param params 
		 * @returns 
		 */
		updateTenant: (id: string, data: object) => {
			return request({
                baseURL: baseAuthUrl,
				url: '/api/multi-tenancy/tenants/' + id,
				method: 'put',
				data
			});
		},

		/**
		 * 删除租户
		 * @param params 
		 * @returns 
		 */
		deleteTenant: (id: string) => {
			return request({
                baseURL: baseAuthUrl,
				url: '/api/multi-tenancy/tenants/' + id,
				method: 'delete'
			});
		}
    };
}
import request from '/@/utils/request';

// 授权链接
const baseAuthUrl: string = import.meta.env.VITE_API_AUTH_URL;

export function useRoleApi() {
    return {
        /**
         * 查询所有的角色信息
         * @returns RoleList
         */
        getRoleAll: () => {
			return request({
				baseURL: baseAuthUrl,
				url: '/api/identity/roles/all',
				method: 'get'
			});
		},

        /**
         * 查询角色(分页)列表信息
         * @param { object }
         * @returns
         */
        getRoleList: (params?: object) => {
			return request({
				baseURL: baseAuthUrl,
				url: '/api/identity/roles',
				method: 'get',
                params
			});
		},

        /**
		 * 添加角色
		 * @param params 
		 * @returns 
		 */
		addRole: (data: object) => {
			return request({
                baseURL: baseAuthUrl,
				url: '/api/identity/roles',
				method: 'post',
				data
			});
		},

		/**
		 * 修改角色
		 * @param params 
		 * @returns 
		 */
		updateRole: (id: string, data: object) => {
			return request({
                baseURL: baseAuthUrl,
				url: '/api/identity/roles/' + id,
				method: 'put',
				data
			});
		},

		/**
		 * 删除角色
		 * @param params 
		 * @returns 
		 */
		deleteRole: (id: string) => {
			return request({
                baseURL: baseAuthUrl,
				url: '/api/identity/roles/' + id,
				method: 'delete'
			});
		}
    };
}
import request from '/@/utils/request';

/**
 * 以下为模拟接口地址，gitee 的不通，就换自己的真实接口地址
 *
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 后端控制菜单模拟json，路径在 https://gitee.com/zhuyongzhengs/abp-micro-server-samples-images/tree/master/menu
 * 后端控制路由，isRequestRoutes 为 true，则开启后端控制路由
 * @method getAdminMenu 获取后端动态路由菜单(admin)
 * @method getTestMenu 获取后端动态路由菜单(test)
 */
export function useMenuApi() {
	return {
		/*
		getAdminMenu: (params?: object) => {
			return request({
				url: '/admin/menu',
				method: 'get',
				params,
			});
		},
		getTestMenu: (params?: object) => {
			return request({
				url: '/gitee/zhuyongzhengs/abp-micro-server-samples-images/raw/master/menu/testMenu.json',
				method: 'get',
				params,
			});
		},
		*/

		/**
		 * 获取角色菜单
		 * @returns
		 */
		getRoleMenu: () => {
			return request({
				url: '/api/base/role-menu/tree',
				method: 'get'
			});
		},

		/**
		 * 获取菜单列表(关键字查询)
		 * @param { object }
		 * @returns
		 */
		getMenuFilter: (params?: object) => {
			return request({
				url: '/api/base/menu/filter',
				method: 'get',
				params
			});
		},

		/**
		 * 获取树形菜单(用于添加|修改时选择上级菜单)
		 * @returns 
		 */
		getMenuTree: () => {
			return request({
				url: '/api/base/menu/tree',
				method: 'get'
			});
		},

		/**
		 * 添加菜单
		 * @param params 
		 * @returns 
		 */
		addMenu: (data: object) => {
			return request({
				url: '/api/base/menu',
				method: 'post',
				data
			});
		},

		/**
		 * 修改菜单
		 * @param params 
		 * @returns 
		 */
		updateMenu: (id: string, data: object) => {
			return request({
				url: '/api/base/menu/' + id,
				method: 'put',
				data
			});
		},

		/**
		 * 删除菜单
		 * @param params 
		 * @returns 
		 */
		deleteMenu: (id: string) => {
			return request({
				url: '/api/base/menu/' + id,
				method: 'delete'
			});
		}
	};
}

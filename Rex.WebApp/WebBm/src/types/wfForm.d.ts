// workflow
declare type RowFormType = {
	id: string;
	api: string;
	description: string;
	disabled: boolean;
	displayName: string;
	fields: string[];
	formName: string;
	namespace: string;
};

interface WfFormStateTableType extends TableType {
	data: RowFormType[];
}

declare interface WfFormState {
	tableData: WfFormStateTableType;
}
